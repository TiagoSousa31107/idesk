﻿using iDesk_Projeto.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iDesk_Projeto.ViewModels
{
    public class CreateRequestModel
    {
        private iDeskEntities db = new iDeskEntities();
        
        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [HiddenInput]
        public int Area { get; set; }

        [Required]
        public SelectList AreaList { get; set; }

        [HiddenInput]
        public int SerialNumber { get; set; }

        [Required]
        public SelectList SerialNumberList { get; set; }

        [HiddenInput]
        public string Priority{get; set;}

        [Required]
        public SelectList PrioritiesList { get; set; }

        public CreateRequestModel (){
            PrioritiesList =new SelectList (db.Priorities, "type", "type");
            AreaList = new SelectList(db.Area, "id", "name");
            SerialNumberList = new SelectList(db.Computers, "serialNumber", "hostname");

        }
    }
}