﻿using Antlr.Runtime.Misc;
using iDesk_Projeto.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace iDesk_Projeto
{
    public class Startup
    {
        public static Func<UserManager<IdentityUser>> UserManagerFactory { get; private set; }

        public void Configuration(IAppBuilder app)
        {

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Login/LogIn")
            });

            // configure the user manager
                UserManagerFactory = () =>
                {
                    var usermanager = new UserManager<IdentityUser>(
                        new UserStore<IdentityUser>(new AppDbContext()));
                    // allow alphanumeric characters in username
                    usermanager.UserValidator = new UserValidator<IdentityUser>(usermanager)
                    {
                        AllowOnlyAlphanumericUserNames = false
                    };

                    return usermanager;
                };
        }
    }
}