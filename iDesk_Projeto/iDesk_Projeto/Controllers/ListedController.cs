﻿using iDesk_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iDesk.Controllers
{
    public class ListedController : Controller
    {
        private iDeskEntities ide = new iDeskEntities();

        //
        // GET: /Listed/Closed

        public ActionResult Closed(string searchTerm = null)
        {
            //Obter o utilizador logado
           // var model = from r in ide.HistoryRequests where r.submitUser == "maria" orderby r.submitDate ascending select r;

            
            
            var model = ide.HistoryRequests
                .OrderBy(r => r.submitDate)
                .Where(r => (searchTerm == null || r.title.Contains(searchTerm)) && r.submitUser == "maria");

            return View(model);
        }

        //
        // GET: /Listed/Pending

        public ActionResult Pending(string searchTerm=null)
        {
            //Obter o utilizador logado
            //var model = from r in ide.Requests where r.submitUser == "maria"  orderby r.submitDate ascending select r;
            var model = ide.Requests
                .OrderBy(r => r.submitDate)
                .Where(r => (searchTerm == null || r.title.Contains(searchTerm) )&& r.submitUser=="maria");
                

            return View(model);
        }


        //
        // GET: /Listed/Pending/Details/5
        public ActionResult Details(int id=-1)
        {
            if(id==-1) return HttpNotFound();

            Requests x = ide.Requests.Find(id);
            if (x.submitUser == "maria") return View(x);
            return HttpNotFound();
        }

        //
        // GET: /Listed/Pending/Details/5
        public ActionResult Edit(int id = -1)
        {
             return View();

        }


    }
}
