﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iDesk_Projeto.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace iDesk_Projeto.Controllers
{
    public class HistoryRequestsController : Controller
    {
        private iDeskEntities db = new iDeskEntities();

        // GET: HistoryRequests
        public ActionResult Index(string searchTerm = null)
        {       
            var user = User.Identity.Name;

            var model = db.HistoryRequests
               .OrderBy(r => r.submitDate)
               .Where(r => (searchTerm == null || r.title.Contains(searchTerm)) && r.submitUser == user);

            return View(model);
        }

        // GET: HistoryRequests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoryRequests historyRequests = db.HistoryRequests.Find(id);
            if (historyRequests == null)
            {
                return HttpNotFound();
            }
            return View(historyRequests);
        }


        // GET: HistoryRequests/Edit/5
        public ActionResult EditHRequest(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HistoryRequests historyRequests = db.HistoryRequests.Find(id);
            if (historyRequests == null)
            {
                return HttpNotFound();
            }

            return View(historyRequests);
        }

        // POST: HistoryRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditHRequest([Bind(Include = "id,submitDate,title,description,closeDate,priority,submitUser,responsible,serialNumber,area")] HistoryRequests historyRequests)
        {

            try{
                historyRequests.closeDate = null;
                if (ModelState.IsValid)
                {
                    db.Entry(historyRequests).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index","Requests");
                   
                 }
            }
              catch
            {
                return Content("Erro ao reabrir o pedido");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
