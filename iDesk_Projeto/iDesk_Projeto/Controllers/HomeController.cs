﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace iDesk_Projeto.Models
{
    public class HomeController : Controller
    {
        protected AppDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        protected UserManager<IdentityUser> UserManager { get; set; }

        public HomeController()
        {
            this.ApplicationDbContext = new AppDbContext();
            this.UserManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this.ApplicationDbContext));
        }

        public async Task<ActionResult> Index()
        {
            //var claimsIdentity = User.Identity as ClaimsIdentity;
            //ViewBag.User = claimsIdentity.FindFirst(ClaimTypes.Name).Value;
            var user = await UserManager.FindByNameAsync(User.Identity.GetUserName());
            return View();
        }
    }
}
