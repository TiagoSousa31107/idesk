﻿using iDesk_Projeto.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using iDesk_Projeto.ViewModels;


namespace iDesk_Projeto.Controllers
{

    [Authorize(Roles = "Administrator")]
    public class BHomeController : Controller
    {

        [ChildActionOnly]
        public ActionResult PendingRequests()
        {
            //Pedidos pendentes do tecnico
             var m = db.Requests
             .OrderBy(r => r.submitDate)
             .Where(r => (r.responsible == User.Identity.Name));

            return PartialView("_Requests", m);
        }

        [ChildActionOnly]
        public ActionResult UnsignedRequests()
        {
            //Pedidos por atribuir
            var m = db.Requests
               .OrderBy(r => r.submitDate)
               .Where(r => (r.responsible == null));

            return PartialView("_Requests", m);
        }

        [ChildActionOnly]
        public ActionResult myHRequests()
        {
            //Pedidos fechados pelo tecnico
            var hr = db.HistoryRequests
             .OrderBy(r => r.submitDate)
            .Where(r => (r.responsible == User.Identity.Name));

            return PartialView("_HRequests", hr);
        }

        [ChildActionOnly]
        public ActionResult allHRequests()
        {
            //Todos os pedidos fechados
            var hr = db.HistoryRequests
             .OrderBy(r => r.submitDate);

            return PartialView("_HRequests", hr);
        }



        private iDeskEntities db = new iDeskEntities();

        // GET: BHome/Dashboard
        public ActionResult Dashboard()
        {

            var m = db.Requests
             .OrderBy(r => r.submitDate);

            ViewBag.Pendentes = m;


            var hr = db.HistoryRequests
           .OrderBy(r => r.submitDate);
            ViewBag.HPendentes = hr;

            ViewData["HPendentes"] = hr;

            return View();
        }

        // GET: BHome/Edit/5
        public ActionResult EditRequest(int id)
        {

            Requests requests = db.Requests.Find(id);
            if (requests == null)
            {
                return HttpNotFound();
            }
            return View(requests);
        }

        // POST: BHome/Edit/5
        [HttpPost]
        public ActionResult EditRequest(Requests requests, int id, FormCollection collection)
        {
            try
            {


                // Se Pedido já tem responsável entao vamos fechar o pedido passando o mesmo para a tabela HistoryRequest
                //esta mudança irá ficar ao cargo de um trigger existente na BD que é responsável por mover o pedido para HistoryRequest quando request.closed=true;
                if (requests.responsible != null)
                {
                    requests.closeDate = DateTime.Now;
                    requests.closed = true;
                }
                //Será assignado o user responsável por resolver o pedido
                else
                {
                    requests.responsible = User.Identity.Name;
                }

                if (ModelState.IsValid)
                {
                    db.Entry(requests).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Dashboard");
            }
            catch
            {
                return Content("Erro ao actualizar o responsável do pedido");
            }
        }


        // GET: BHome/Edit/5
        public ActionResult EditHRequest(int id)
        {
            HistoryRequests hRequest = db.HistoryRequests.Find(id);
            if (hRequest == null)
            {
                return HttpNotFound();
            }
            return View(hRequest);
        }

        // POST: BHome/Edit/5
        [HttpPost]
        public ActionResult EditHRequest(HistoryRequests hreq, int id)
        {
            try
            {
               
                hreq.closeDate = null;
                hreq.responsible = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    db.Entry(hreq).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Dashboard");
            }
            catch
            {
                return Content("Erro ao reabrir o pedido");
            }

        }

        public ActionResult Statistics()
        {

            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(String Pesquisa)
        {
            try
            {
                return RedirectToAction("Dashboard");
            }
            catch
            {
                return Content("Erro ao actualizar o responsável do pedido");
            }

        }

        public ActionResult Settings()
        {
            return View();
        }

        public ActionResult Tickets()
        {
            return View(new CreateRequestModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Tickets([Bind(Include = "title,description,serialNumber,area,priority")] CreateRequestModel newRequest)
        {
              var request = new Requests()
            {
                submitUser = User.Identity.Name,
                submitDate = DateTime.Now,
                closed = false,
                title = newRequest.Title,
                description = newRequest.Description,
                serialNumber = newRequest.SerialNumber,
                area = newRequest.Area,
                priority = newRequest.Priority
            };

            if (ModelState.IsValid)
            {
                db.Requests.Add(request);
                db.SaveChanges();
                return RedirectToAction("Dashboard");
            }

            return View(newRequest);
        }

    }
}
