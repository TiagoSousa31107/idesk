﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iDesk_Projeto.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using iDesk_Projeto.ViewModels;

namespace iDesk_Projeto.Controllers
{

   // [Authorize]
    public class RequestsController : Controller
    {
        private iDeskEntities db = new iDeskEntities();
       
        
        //protected AppDbContext ApplicationDbContext { get; set; }
        //protected UserManager<IdentityUser> UserManager { get; set; }

        //public RequestsController() {

        //    this.ApplicationDbContext = new AppDbContext();
        //    this.UserManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this.ApplicationDbContext));
        //}


        // GET: Requests
        public ActionResult Index(string searchTerm = null )
        {

            string user = User.Identity.Name;


            var model = db.Requests
                .OrderBy(r => r.submitDate)
                .Where(r => (searchTerm == null || r.title.Contains(searchTerm)) && r.submitUser == user);


            return View(model);
        }

        // GET: Requests/Details/5
        public ActionResult Details(int? id)
        {
            string user = User.Identity.GetUserName();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null || requests.submitUser!=user)
            {
                return HttpNotFound();
            }
            return View(requests);
        }

        // GET: Requests/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            ViewBag.area = new SelectList(db.Area, "id", "name");
            ViewBag.serialNumber = new SelectList(db.Computers, "serialNumber", "hostname");
            ViewBag.priority = new SelectList(db.Priorities, "type", "type");
            return View(new CreateRequestModel());
        }

        // POST: Requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "title,description,serialNumber,area,priority")] CreateRequestModel newRequest)
        {
                var request = new Requests()
                {
                    submitUser = User.Identity.Name,
                    submitDate = DateTime.Now,
                    closed = false,
                    title = newRequest.Title,
                    description = newRequest.Description,
                    serialNumber = newRequest.SerialNumber,
                    area = newRequest.Area,
                    priority = newRequest.Priority
                };

            if (ModelState.IsValid)
            {
                db.Requests.Add(request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newRequest);
        }

        // GET: Requests/Edit/5
        public ActionResult EditRequest(int? id)
        {

            string user = User.Identity.Name;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null || requests.submitUser!=user)
            {
                return HttpNotFound();
            }
            ViewBag.area = new SelectList(db.Area, "id", "name", requests.area);
            ViewBag.serialNumber = new SelectList(db.Computers, "serialNumber", "hostname", requests.serialNumber);
            ViewBag.priority = new SelectList(db.Priorities, "type", "type", requests.priority);
            ViewBag.responsible = new SelectList(db.Users, "username", "name", requests.responsible);
            ViewBag.submitUser = new SelectList(db.Users, "username", "username", requests.submitUser);
            return View(requests);
        }

        // POST: Requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRequest([Bind(Include = "id,submitDate,title,description,closeDate,closed,serialNumber,area,priority,submitUser,responsible")] Requests requests)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requests).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.area = new SelectList(db.Area, "id", "name", requests.area);
            ViewBag.serialNumber = new SelectList(db.Computers, "serialNumber", "hostname", requests.serialNumber);
            ViewBag.priority = new SelectList(db.Priorities, "type", "type", requests.priority);
            ViewBag.responsible = new SelectList(db.Users, "username", "name", requests.responsible);
            ViewBag.submitUser = new SelectList(db.Users, "username", "username", requests.submitUser);
            return View(requests);
        }

        // GET: Requests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null)
            {
                return HttpNotFound();
            }
            return View(requests);
        }

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Requests requests = db.Requests.Find(id);
            db.Requests.Remove(requests);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
